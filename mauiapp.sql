-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 11, 2015 at 10:53 PM
-- Server version: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `mauiapp`
--

-- --------------------------------------------------------

--
-- Table structure for table `mss_branches`
--

DROP TABLE IF EXISTS `mss_branches`;
CREATE TABLE IF NOT EXISTS `mss_branches` (
  `branch_id` int(10) unsigned NOT NULL,
  `store_id` int(11) NOT NULL,
  `address` text COLLATE utf8_unicode_ci NOT NULL,
  `landmark` text COLLATE utf8_unicode_ci NOT NULL,
  `store_hours` text COLLATE utf8_unicode_ci NOT NULL,
  `telephone_number` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_branches`
--

INSERT INTO `mss_branches` (`branch_id`, `store_id`, `address`, `landmark`, `store_hours`, `telephone_number`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, '199 E. Alex Bell Rd. Unit 402, Centerville Ohio 45459', 'next to Doubleday''s and Firestone', '', '(937) 401-0710', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(2, 2, '7967 Cincinnati Dayton Rd Unit J, West Chester Ohio 45069', '', '', '(513) 847-6008', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(3, 3, '3092 Madison Rd., Cincinnati Ohio 45209', '', '', '(513) 788-2212', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(4, 4, '9247 E 141 st. Fishers Indiana 46038', '', '', '(317) 214-7829', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(5, 5, '8660 Guion Rd, Indianapolis Indiana 46268', '', '', '(317) 451-4212', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(6, 6, '4113 Oechsli Ave Suite F, Louisville Kentucky 40207', '', '', '(502) 938-MAUI or (502) 938-6284', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(7, 7, '330 Louisiana Ave. Suite E, Perrysburg Ohio 43551', '', '', '(419) 265-0699', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(8, 8, '97 N. Kingshighway Suite 6, Cape Girardeau Missouri 63701', '', '', '573-803-1818', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(9, 9, 'Eastern Hills Mall 4545 Transit Rd, Williamsville New York 14221', '', '', '(716) 580-7066', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL),
(10, 10, '12751 Marblestone Drive, Suite 240, Woodbridge Virginia 22192', '', '', '(703) 730-6284', 'active', '2015-08-06 04:00:00', '2015-08-06 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_check_ins`
--

DROP TABLE IF EXISTS `mss_check_ins`;
CREATE TABLE IF NOT EXISTS `mss_check_ins` (
  `check_in_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_no` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `no_of_guest` smallint(5) unsigned NOT NULL,
  `is_check_in` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no',
  `check_in_at` timestamp NULL DEFAULT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=106 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_check_ins`
--

INSERT INTO `mss_check_ins` (`check_in_id`, `branch_id`, `service_id`, `name`, `email`, `phone_no`, `no_of_guest`, `is_check_in`, `check_in_at`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(101, 9, 2, 'zhan', 'zhan@molavenet.com', '12345', 1, 'no', NULL, 'active', '2015-11-03 21:02:25', '2015-11-03 21:02:25', NULL),
(102, 9, 2, 'Mark Joseph', 'mjoseph@gmail.com', '098173', 1, 'no', NULL, 'active', '2015-11-03 23:04:51', '2015-11-03 23:04:51', NULL),
(103, 9, 2, 'Zhandra Habig', 'zhan@yahoo.com', '09183634', 1, 'no', NULL, 'active', '2015-11-04 22:05:45', '2015-11-04 22:05:45', NULL),
(104, 1, 1, 'Marc Jesus', 'mj@yahoo.com', '1234567889', 1, 'no', NULL, 'active', '2015-11-06 17:56:59', '2015-11-06 17:56:59', NULL),
(105, 1, 1, 'Juan', 'juan@two.com', '213456', 1, 'no', NULL, 'active', '2015-11-06 17:58:52', '2015-11-06 17:58:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_migrations`
--

DROP TABLE IF EXISTS `mss_migrations`;
CREATE TABLE IF NOT EXISTS `mss_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_migrations`
--

INSERT INTO `mss_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1),
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2015_08_09_232147_create_posts_table', 1),
('2015_08_14_192536_create_branches_table', 1),
('2015_08_15_230000_create_check_in_table', 1),
('2015_08_15_232437_create_services_table', 1),
('2015_08_15_232843_create_services_offer_table', 1),
('2015_08_15_233145_create_stores_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mss_password_resets`
--

DROP TABLE IF EXISTS `mss_password_resets`;
CREATE TABLE IF NOT EXISTS `mss_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mss_posts`
--

DROP TABLE IF EXISTS `mss_posts`;
CREATE TABLE IF NOT EXISTS `mss_posts` (
  `post_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_posts`
--

INSERT INTO `mss_posts` (`post_id`, `user_id`, `content`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 'tex\r\n', 'active', '2015-08-19 12:36:58', '2015-08-19 12:36:58', NULL),
(2, 1, 'data\r\n', 'active', '2015-08-20 09:44:51', '2015-08-20 09:44:51', NULL),
(3, 1, 'help', 'active', '2015-08-20 09:45:57', '2015-08-20 09:45:57', NULL),
(4, 1, 'test', 'active', '2015-08-20 09:46:41', '2015-08-20 09:46:41', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services`
--

DROP TABLE IF EXISTS `mss_services`;
CREATE TABLE IF NOT EXISTS `mss_services` (
  `service_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price` double(8,2) NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_services`
--

INSERT INTO `mss_services` (`service_id`, `name`, `price`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Laser Teeth Whitening', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(2, 'Eyelash Extensions', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(3, 'Medical Weight Loss', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL),
(4, 'Botox/Fillers', 0.00, '1', '2015-08-07 04:00:00', '2015-08-07 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_services_offer`
--

DROP TABLE IF EXISTS `mss_services_offer`;
CREATE TABLE IF NOT EXISTS `mss_services_offer` (
  `service_offer_id` int(10) unsigned NOT NULL,
  `service_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_services_offer`
--

INSERT INTO `mss_services_offer` (`service_offer_id`, `service_id`, `branch_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 2, 1, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 3, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 4, 2, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 1, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 3, 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 2, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 4, 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 1, 5, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 1, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(11, 2, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(12, 4, 6, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(13, 1, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(14, 4, 7, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(15, 3, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(16, 4, 8, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(17, 2, 9, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(18, 1, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(19, 3, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(20, 4, 10, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_stores`
--

DROP TABLE IF EXISTS `mss_stores`;
CREATE TABLE IF NOT EXISTS `mss_stores` (
  `store_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_stores`
--

INSERT INTO `mss_stores` (`store_id`, `name`, `user_id`, `status`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'Maui Whitening Dayton', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(2, 'Maui Whitening West Chester', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(3, 'Maui Whitening Hyde Park / Oakley', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(4, 'Maui Whitening Fishers', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(5, 'Maui Whitening at Indy Health & Fitness', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(6, 'Maui Whitening Louisville', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(7, 'Maui Whitening Perrysburg', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(8, 'Maui Whitening Cape Girardeau', 3, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(9, 'Maui Whitening Williamsville', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL),
(10, 'Maui Whitening Prince William', 4, 'active', '2015-08-15 04:00:00', '2015-08-15 04:00:00', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_stores_availability`
--

DROP TABLE IF EXISTS `mss_stores_availability`;
CREATE TABLE IF NOT EXISTS `mss_stores_availability` (
  `store_availability_id` int(10) unsigned NOT NULL,
  `store_id` int(10) unsigned NOT NULL,
  `branch_id` int(10) unsigned DEFAULT NULL,
  `user_id` int(10) unsigned DEFAULT NULL,
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `available_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_stores_availability`
--

INSERT INTO `mss_stores_availability` (`store_availability_id`, `store_id`, `branch_id`, `user_id`, `status`, `available_at`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 1, 1, 1, 'active', '2015-10-01 00:18:01', '2015-09-30 23:48:02', '2015-09-30 23:48:02', NULL),
(2, 1, 1, 1, 'active', '2015-10-01 02:14:08', '2015-10-01 01:14:08', '2015-10-01 01:14:08', NULL),
(3, 0, 4, 0, 'active', '2015-10-06 17:35:00', '2015-10-06 17:20:00', '2015-10-06 17:20:00', NULL),
(4, 0, 6, 0, 'active', '2015-10-06 17:36:07', '2015-10-06 17:21:07', '2015-10-06 17:21:07', NULL),
(5, 0, 6, 0, 'active', '2015-10-06 19:21:18', '2015-10-06 17:21:18', '2015-10-06 17:21:18', NULL),
(35, 0, 1, 0, 'active', '2015-10-16 18:53:01', '2015-10-16 17:53:01', '2015-10-16 17:53:01', NULL),
(36, 0, 1, 0, 'active', '2015-10-31 01:24:31', '2015-10-31 01:09:31', '2015-10-31 01:09:31', NULL),
(37, 0, 1, 0, 'active', '2015-10-31 01:24:32', '2015-10-31 01:09:32', '2015-10-31 01:09:32', NULL),
(38, 0, 1, 0, 'active', '2015-10-31 01:26:21', '2015-10-31 01:11:21', '2015-10-31 01:11:21', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mss_users`
--

DROP TABLE IF EXISTS `mss_users`;
CREATE TABLE IF NOT EXISTS `mss_users` (
  `user_id` int(10) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `role` enum('user','admin') COLLATE utf8_unicode_ci DEFAULT 'user',
  `status` varchar(16) COLLATE utf8_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `mss_users`
--

INSERT INTO `mss_users` (`user_id`, `name`, `email`, `password`, `remember_token`, `role`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Jemuel Rodriguez', 'jemuel@primeoutsourcing.com', '$2y$10$Q3qjZGAg7TnONy1YzcvX5ObNHUxsrFoBUOYbj2o7evNg8K5DKpuIi', '9IuJI0FKeoV0H3z5V28lOsY5ORh5RAOqsYT0vGtjjGGWNWQrvUqfn19Fi3Nz', 'admin', 'active', '2015-08-19 09:42:44', '2015-11-11 02:12:19'),
(2, 'Dummy Accnt', 'dummyaccnt548@gmail.com', '$2y$10$CP18b7WkU6HAVv27/pc.TOidtf1vO71qsXxKjlizncGSV8eEOl9h2', NULL, 'user', 'active', '2015-08-24 05:09:17', '2015-08-24 05:09:17'),
(3, 'MJ', 'mj@molavenet.com', '$2y$10$Q3qjZGAg7TnONy1YzcvX5ObNHUxsrFoBUOYbj2o7evNg8K5DKpuIi', 'tK75m1JZ0j17xTuw4BRswm3EpYpnPxr5j0VQXLmyfEFSMQK6IaFQTK1crYm6', 'user', 'active', '2015-08-19 09:42:44', '2015-09-15 23:55:17'),
(4, 'mark', 'marquez@molavenet.com', '$2y$10$v5FiTyrCovM8lw4IJ32G..8qSDKt.Me5GREbXd1BfYYKntjQioxz6', NULL, 'user', 'active', '2015-09-17 19:30:33', '2015-09-17 19:30:33');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mss_branches`
--
ALTER TABLE `mss_branches`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `mss_check_ins`
--
ALTER TABLE `mss_check_ins`
  ADD PRIMARY KEY (`check_in_id`);

--
-- Indexes for table `mss_password_resets`
--
ALTER TABLE `mss_password_resets`
  ADD KEY `password_resets_email_index` (`email`), ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `mss_posts`
--
ALTER TABLE `mss_posts`
  ADD PRIMARY KEY (`post_id`);

--
-- Indexes for table `mss_services`
--
ALTER TABLE `mss_services`
  ADD PRIMARY KEY (`service_id`);

--
-- Indexes for table `mss_services_offer`
--
ALTER TABLE `mss_services_offer`
  ADD PRIMARY KEY (`service_offer_id`);

--
-- Indexes for table `mss_stores`
--
ALTER TABLE `mss_stores`
  ADD PRIMARY KEY (`store_id`);

--
-- Indexes for table `mss_stores_availability`
--
ALTER TABLE `mss_stores_availability`
  ADD PRIMARY KEY (`store_availability_id`);

--
-- Indexes for table `mss_users`
--
ALTER TABLE `mss_users`
  ADD PRIMARY KEY (`user_id`), ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mss_branches`
--
ALTER TABLE `mss_branches`
  MODIFY `branch_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mss_check_ins`
--
ALTER TABLE `mss_check_ins`
  MODIFY `check_in_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=106;
--
-- AUTO_INCREMENT for table `mss_posts`
--
ALTER TABLE `mss_posts`
  MODIFY `post_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mss_services`
--
ALTER TABLE `mss_services`
  MODIFY `service_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `mss_services_offer`
--
ALTER TABLE `mss_services_offer`
  MODIFY `service_offer_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `mss_stores`
--
ALTER TABLE `mss_stores`
  MODIFY `store_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `mss_stores_availability`
--
ALTER TABLE `mss_stores_availability`
  MODIFY `store_availability_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `mss_users`
--
ALTER TABLE `mss_users`
  MODIFY `user_id` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
