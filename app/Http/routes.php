<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', 'ContentController@index');

// Dashboard
Route::get('dashboard', 'DashboardController@index');

// Authentication
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

// Branches
Route::get('branches/create', 'BranchController@create');
Route::post('branches/store', 'BranchController@store');
Route::get('branches', 'BranchesController@index');

// Check Ins
Route::get('check-ins', 'CheckInsController@index');

// Services
Route::get('services/create', 'ServicesController@create');
Route::post('services/store', 'ServicesController@store');

// Users
Route::get('users', 'UsersController@index');
Route::get('users/{userId}', 'UsersController@show');
Route::get('users/{userId}/stores', 'UsersController@storeShow');
Route::get('users/{userId}/stores/{store}', 'UsersController@store');
Route::get('users/{userId}/branches/{branchId}', 'UsersController@checkinShow');

// Search
Route::get('/search/keyword?={keyword}', 'DashboardController@search');

/*
|--------------------------------------------------------------------------
| Restful Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for restful api.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
// Braches
Route::get('rest/branches', 'RestfulController@showBranches');
Route::get('rest/branches/{id}', 'RestfulController@showBranches');

// Services
Route::get('rest/services', 'RestfulController@showServices');
Route::get('rest/services/{id}', 'RestfulController@showServices');

// Stores
Route::get('rest/stores', 'RestfulController@showStores');
Route::get('rest/stores/{id}', 'RestfulController@showStores');

// Services
Route::get('rest/services-offer', 'RestfulController@showServicesOffer');
Route::get('rest/services-offer/{id}', 'RestfulController@showServicesOffer');
Route::get('rest/services-with-branches/{id}', 'RestfulController@showServicesWithBranches');

// Check Ins
Route::post('rest/check-ins', 'RestfulController@storeCheckIns');
Route::get('rest/check-ins/{id}', 'RestfulController@showCheckIns');

// Login or authentication
Route::post('rest/authenticate', 'RestfulController@authenticate');
Route::get('rest/users-with-stores/{id}', 'RestfulController@showUsersWithStores');

// Stores Availability
Route::post('rest/stores-availability', 'RestfulController@storeStoresAvailability');
Route::get('rest/stores-availability/{branchId}', 'RestfulController@showStoresAvailability');

//