<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CreateBranchRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'store_id' => 'required',
            'address'  => 'required|min:3',
            // 'landmark' => 'required|min:3',
            // 'store_hours' => 'required|min:3',
            'telephone_number' => 'required|min:3',
        ];
    }
}