<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App;
use Auth;
use Carbon;
use DB;
use Validator;
use App\Http\Requests;
use App\Http\Requests\CreateCheckInRequest;
use App\Http\Controllers\Controller;

class RestfulController extends Controller
{
    

    /**
     * Json return header
     *
     * @return Response
     */
    public function jsonFileHeader() 
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Allow-Methods: GET, POST, OPTIONS');
        header('Access-Control-Allow-Headers: Origin, Content-Type, Accept, Authorization, X-Request-With');
        header('Access-Control-Allow-Credentials: true');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return Response
     */
    public function authenticate(Request $request)
    {   
        $email      = $request->email;
        $password   = $request->password;

        if (Auth::attempt(['email' => $email, 'password' => $password])) {
            $user = Auth::user();
            $response = [
                'authenticate'  => 'true', 
                'user_id'       => $user->user_id, 
                'email'         => $user->email, 
                'name'          => $user->name
            ];
        } else {
            $response = ['authenticate' => 'false'];
        }
        
        // Return the json file header.
        $this->jsonFileHeader();
        return $response;
    }


    /**
     * Display the _branches.
     *
     * @return Response
     */
    public function showBranches($id = null)
    {
        $branchesWithStores = DB::table('branches AS b')
        ->leftJoin('stores AS st', 'st.store_id', '=', 'b.store_id')
        ->select(
            'b.branch_id',
            'b.store_id',
            'b.address', 
            'b.landmark', 
            'b.store_hours', 
            'b.telephone_number',
            'st.name AS store_name'
        )
        ->where('b.branch_id', $id)
        ->get();

        $branchesWithServices = DB::table('services_offer AS so')
        ->leftJoin('services AS s', 'so.service_id', '=', 's.service_id')
        ->select('s.service_id', 's.name')
        ->where('so.branch_id', $id)
        ->get();        

        $brachesWithStoresAvailabilities = App\StoresAvailability::where('branch_id', $id)
        ->orderBy('store_availability_id', 'desc')
        ->first();

        if ($brachesWithStoresAvailabilities) {
            $storesAvailabilities = [
                'available_at'      => $brachesWithStoresAvailabilities->available_at,
                'available_at_human'=> $brachesWithStoresAvailabilities->available_at->diffForHumans()
            ];
        } else {
            $storesAvailabilities = [];
        }

        $branchesWithStoresAndServices = array(
            'branches' => $branchesWithStores, 
            'services' => $branchesWithServices,
            'store_availabilities' => $storesAvailabilities
        );

        // Return the json file header
        $this->jsonFileHeader();

        return $branchesWithStoresAndServices;
    }
    

    /**
     * Display the _services.
     *
     * @return Response
     */
    public function showServices($id = null)
    {   
        if (empty($id)) {
            $services = App\Services::all();
        } else {
            $services = App\Services::findOrFail($id);
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $services;
    }


    /**
     * Display the _stores.
     *
     * @return Response
     */
    public function showStores($id = null)
    {
        
        if (empty($id)) {
            $stores = App\Stores::all();    
        } else {
            $stores = App\Stores::findOrFail($id);
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $stores;
    }


    /**
     * Display the _branches which associated with _services.
     *
     * @var $id unique key of _services
     * @todo It should be eloquent not \DB
     * @return Response
     */
    public function showServicesWithBranches($id = null)
    {

        $servicesWithBranches = DB::table('services_offer AS so')
        ->join('branches AS b', 'so.branch_id', '=', 'b.branch_id')
        ->leftJoin('stores AS st', 'st.store_id', '=', 'b.store_id')
        ->leftJoin('services AS s', 'so.service_id', '=', 's.service_id')
        ->select(
            'b.branch_id',
            'b.store_id',
            'b.address', 
            'b.landmark', 
            'b.store_hours', 
            'b.telephone_number', 
            's.name AS service_name', 
            'st.name AS store_name'
        )
        ->where('so.service_id', $id)
        ->get();

        // Get the _store_availability
        foreach ($servicesWithBranches as $servicesWithBranch) {

            // Elequent
            $storesAvailability = App\StoresAvailability::where('branch_id', $servicesWithBranch->branch_id)
            ->orderBy('store_availability_id', 'desc')
            ->first();

            $availableAt = '';
            $availableAtReadable = '';
            if ($storesAvailability != null) {
                $availableAt = $storesAvailability->available_at;
                $availableAtReadable =  $storesAvailability->created_at->diffForHumans();
            }

            $data[] = array(
                'branch_id'         => $servicesWithBranch->branch_id,
                'store_id'          => $servicesWithBranch->store_id,
                'address'           => $servicesWithBranch->address, 
                'landmark'          => $servicesWithBranch->landmark, 
                'store_hours'       => $servicesWithBranch->store_hours, 
                'telephone_number'  => $servicesWithBranch->telephone_number, 
                'service_name'      => $servicesWithBranch->service_name, 
                'store_name'        => $servicesWithBranch->store_name,
                'available_at'      => $availableAt,
                'available_at_human'=> $availableAtReadable
            );
        }
        
        // Return the json file header.
        $this->jsonFileHeader();
        return $data;
    }

    
    /**
     * Store a newly created check_ins
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeCheckIns(Request $request)
    {
        // Validate the data.
        // Used manual validator intead of Requests/CheckInsRequest so we can return the response to json.
        $validator = Validator::make($request->all(), [
            'branch_id'  => 'required',
            'service_id' => 'required',
            'name'       => 'required|min:3',
            'email'      => 'required|email',
            'phone_no'   => 'required|min:3',
            'no_of_guest'=> 'required|numeric',

        ]);

        // If validator fails.
        if ($validator->fails()) {
            $response = ['status' => 'failed', 'message' => $validator->errors()];
        // No errors, just continue.
        } else {

            $checkIns = new App\CheckIns;
            $checkIns->branch_id    = $request->branch_id;
            $checkIns->service_id   = $request->service_id;
            $checkIns->name         = $request->name;
            $checkIns->email        = $request->email;
            $checkIns->phone_no     = $request->phone_no;            
            $checkIns->no_of_guest  = $request->no_of_guest;

            if ($checkIns->save()) {
                $response = ['status' => 'success', 'message' => 'Successfully Check In!'];
            } else {
                $response = ['status' => 'failed', 'message' => 'Database Error'];
            }
        }

        // Return the json file header.
        $this->jsonFileHeader();
        return $response;
    }


    /**
     * Display a newly created _check_ins
     * 
     * @param  
     * @param  Request $request
     * @return Response
     */
    public function showCheckIns($branchId = null)
    {
        
        $data = '';
        // $checkIns = '';
        if (!empty($branchId)) {
            $checkIns = App\CheckIns::where('branch_id', $branchId)
            ->orderBy('created_at', 'desc')
            ->get();

            if ($checkIns) {

                foreach ($checkIns as $checkIn) {
                    
                    $branchesWithServices = DB::table('services_offer AS so')
                    ->leftJoin('services AS s', 'so.service_id', '=', 's.service_id')
                    ->select('s.name AS service_name')
                    ->where('so.service_id', $checkIn->service_id)
                    ->first();

                    $data[] = array(
                        'check_in_id'   => $checkIn->check_in_id,
                        'branch_id'     => $checkIn->branch_id,
                        'service_id'    => $checkIn->service_id,
                        'service_name'  => $branchesWithServices->service_name,
                        'name'          => $checkIn->name,
                        'email'         => $checkIn->email,
                        'phone_no'      => $checkIn->phone_no,
                        'no_of_guest'   => $checkIn->no_of_guest,
                        'check_in_at'   => $checkIn->created_at,
                        'check_in_at_human' => $checkIn->created_at->diffForHumans()
                    );
                }
            } else {
                $data = ['status' => 'failed', 'message' => 'branch_id not found'];
            }
        }
        
        $response = $data;
        
        // Return the json file header.
        $this->jsonFileHeader();
        return $response;
    }


    /**
     * Display the _users which associated with _stores.
     * 
     * @return 
     */
    public function showUsersWithStores($id = null)
    {
        $usersWithStores = DB::table('users AS u')
        ->join('stores AS s', 'u.user_id', '=', 's.user_id')
        ->join('branches AS b', 'b.store_id', '=', 's.store_id')
        ->select(
            'u.user_id',
            's.store_id',
            's.name AS store_name',
            'b.address',
            'b.landmark',
            'b.telephone_number'
        )
        ->where('u.user_id', $id)
        ->get();

        // Return the json file header.
        $this->jsonFileHeader();
        return $usersWithStores;
    }


    /**
     * Store a newly created stores_availability
     *
     * @param  Request  $request
     * @return Response
     */
    public function storeStoresAvailability(Request $request)
    {

        // Validate the data.
        // Used manual validator intead of Requests/CheckInsRequest so we can return the response to json.
        $validator = Validator::make($request->all(), [
            'store_id'  => 'required',
            'branch_id' => 'required',
            'user_id'   => 'required',
            'available_at' => 'required|numeric',
        ]);

        // If validator fails.
        if ($validator->fails()) {
            $response = ['status' => 'failed', 'message' => $validator->errors()];
        // No errors, just continue.
        } else {

            $minutes = $request->available_at;
            $dateNow = Carbon\Carbon::now();
            $availableAt = $dateNow->addMinutes($minutes);

            $storesAvailability = new App\StoresAvailability;
            $storesAvailability->store_id     = $request->store_id;
            $storesAvailability->branch_id    = $request->branch_id;
            $storesAvailability->user_id      = $request->user_id;
            $storesAvailability->available_at = $availableAt;

            if ($storesAvailability->save()) {
                $response = ['status' => 'success', 'message' => 'Success!'];
            } else {
                $response = ['status' => 'failed', 'message' => 'Database Error'];
            }
        }

        // Return the json file header.
        $this->jsonFileHeader();

        return $response;
    }


    /**
     * Store a newly created stores_availability
     *
     * @param  Request  $request
     * @return Response
     */
    public function showStoresAvailability($branchId)
    {
        
        // Return the json file header.
        $this->jsonFileHeader();
        return $response;
    }
}