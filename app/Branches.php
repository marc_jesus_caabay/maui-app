<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Branches extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branches';
    

    /**
     * This a primary key
     *
     * @var string
     */
    protected $primaryKey = 'branch_id';

   
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['store_id', 'address', 'landmark', 'store_hours', 'telephone_number'];


    /**
     * Get the _stores that owns the _branches.
     */
    public function stores()
    {
        return $this->belongsTo('App\Stores', 'store_id');
    }

}
