<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Mosaddek">
    <meta name="keyword" content="FlatLab, Dashboard, Bootstrap, Admin, Template, Theme, Responsive, Fluid, Retina">
    <link rel="shortcut icon" href="img/favicon.png">
    <title>Maui Super Saloon</title>
    <!-- Bootstrap core CSS -->
    <link href="{{ url() }}/themes/flatlab/css/bootstrap.min.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/bootstrap-reset.css" rel="stylesheet">
    <!--external css-->
    <link href="{{ url() }}/themes/flatlab/assets/font-awesome/css/font-awesome.css" rel="stylesheet" />
    <link href="{{ url() }}/themes/flatlab/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.css" rel="stylesheet" type="text/css" media="screen"/>
    <link rel="stylesheet" href="{{ url() }}/themes/flatlab/css/owl.carousel.css" type="text/css">
    <!--right slidebar-->
    <link href="{{ url() }}/themes/flatlab/css/slidebars.css" rel="stylesheet">
    <!-- Custom styles for this template -->
    <link href="{{ url() }}/themes/flatlab/css/style.css" rel="stylesheet">
    <link href="{{ url() }}/themes/flatlab/css/style-responsive.css" rel="stylesheet" />
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 tooltipss and media queries -->
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <script src="{{ url() }}/themes/flatlab/js/jquery.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/count.js"></script>
</head>
<body>
    <section id="container" >
        @yield('content') 
    </section>
    <!-- js placed at the end of the document so the pages load faster -->
    
    <script src="{{ url() }}/themes/flatlab/js/bootstrap.min.js"></script>
    <script class="include" type="text/javascript" src="{{ url() }}/themes/flatlab/js/jquery.dcjqaccordion.2.7.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/jquery.scrollTo.min.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/jquery.nicescroll.js" type="text/javascript"></script>
    <script src="{{ url() }}/themes/flatlab/js/jquery.sparkline.js" type="text/javascript"></script>
    <script src="{{ url() }}/themes/flatlab/assets/jquery-easy-pie-chart/jquery.easy-pie-chart.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/owl.carousel.js" ></script>
    <script src="{{ url() }}/themes/flatlab/js/jquery.customSelect.min.js" ></script>
    <script src="{{ url() }}/themes/flatlab/js/respond.min.js" ></script>
    <!--right slidebar-->
    <script src="{{ url() }}/themes/flatlab/js/slidebars.min.js"></script>
    <!--common script for all pages-->
    <script src="{{ url() }}/themes/flatlab/js/common-scripts.js"></script>
    <!--script for this page-->
    <script src="{{ url() }}/themes/flatlab/js/sparkline-chart.js"></script>
    <script src="{{ url() }}/themes/flatlab/js/easy-pie-chart.js"></script>
    
    <script>
        //owl carousel
        $(document).ready(function() {
            $("#owl-demo").owlCarousel({
                navigation : true,
                slideSpeed : 300,
                paginationSpeed : 400,
                singleItem : true,
                autoPlay:true
            });
        });

        //custom select box
        $(function(){
            $('select.styled').customSelect();
        });
    </script>
</body>
</html>